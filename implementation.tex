%!TEX root = ./main.tex

\section{Prototype architecture}
\label{sec:implementation}

We developed a prototype called VNET that supports an initial number of
SARNET control loop elements, with particular focus on \textit{Detect,
    Decide, Respond, Measure,} and \textit{Learn} phases.
Currently, VNET is able to provide a visualisation of a network suffering
from basic DDoS attacks and it allows users to manipulate the network
characteristics with direct visual feedback on their actions
and the effects thereof. It allows the creation of simple observables based on the current state of the network topology, traffic and elements.
Additionally, VNET allows scripting of attack scenarios, which execute
network changes using the network controller. Real-time monitoring data and
observable states are forwarded to the UI for visualization and user
interaction. Fig.~\ref{fig:vnet-components} shows the application
components of the VNET. 

% In a fully fledged SARNET implementation the network topology where the
% services are running is the result of the SARNET bootstrapping procedure.
% The requirement of the applications are compiled into a network topology that
% has the smallest possible attack surface.

%In the \textit{Detect} phase we present the value of the security observables,
%which are being monitored continuously.  In this case these are the network
%state and the service state.  The network state is bandwidth capacity and the
%current traffic rate and the state of the links.  The service state is defined
%by the number of successful customer transactions and the associated revenue.
%We define the normal values for each of the security observables; attacks on
%the network will result in violations of the acceptable values.  The
%\textit{Decide} phase was supported by a UI specifically designed for this
%purpose (see Sec.~\ref{sec:touchtable}).
%During the \textit{React} phase users interface to system to adjust all the
%controllable network parameters,
%e.g. disable links, adjust link capacity, and provide appropriate
%protocol filters.
%The effectiveness of the user response is measured immediately after the
%application of a countermeasure.

%We used a database to store the solutions chosen by the users when the system
%was under attack. The database is the central component required in the
%\textit{Learn} phase of the control loop.
%All solutions known are then used in subsequent \textit{Decide} phases, such
%that the system can react quickly and autonomously.

\begin{figure}[h]
    \centering
    \includegraphics[width=8cm]{img/components.pdf}
    \caption{Software components in the VNET prototype.}
    \label{fig:vnet-components}
\end{figure}



\subsection{ExoGENI}

As initial underlying platform for the VNET operations, we used ExoGENI
\cite{baldine2012exogeni}.
ExoGENI is a platform for orchestrating cloud resources (OpenStack), SDNs
(OpenFlow)\cite{mckeown2008openflow}, and network
circuits\cite{baldine2010networked}.
There are currently about 20 ExoGENI racks operational at various educational and
research institutes, including one located at the University of Amsterdam
(the \textit{uva-nl rack}).
Resources are deployed in the form of slices and can span multiple racks and
networks (physical sites).
The platform hides the hardware differences between the racks and automatically
configures the network.
Network elements and functions are implemented as % XXX; XOsmall / smallest size
virtual machines.




\subsection{Network functions}

Independence of the underlying infrastructure was an important design
requirement.
Therefore,
we use Ansible\footnote{Ansible: \url{https://www.ansible.com/}} playbooks to build our network functions and to prepare the
necessary VM images.
These playbooks contain instructions to install each virtual machine and
to configure the required software packages, including the VNET agent
which we use to monitor the VM.
We currently have network functions for traditional and OpenFlow switches,
RIP and OSPF routers, and SDN controllers such as OpenDaylight and
Ryu \cite{khondoker2014feature}. These network functions are used in the
network topologies that are deployed by VNET.
% Maybe something about bootstrapping



\subsection{Infrastructure controller}

The infrastructure controller acquires and monitors the topology from the underlying infrastructure.
This topology consists of the VMs and virtual network links.
It translates the topology data from the Infrastructure as a Service
(IaaS) controller and converts this to the VNET internal format.
The topology is regularly polled at a tunable rate dependent on the expected
frequency of changes.
A push based approach can also be used if the IaaS controller
supports custom plugins or sending topology updates.
% In our case we used the ExoGENI platform to provide the virtual machines and
% the virtual network devices and links (see Sec.~\ref{sec:exogeni}).


\subsection{Monitoring controller}

%As stated previously the value of the security observables determines the state of the network.
The purpose of the monitoring controller is to collect information, metrics,
and statistics from the nodes and links in the network and to pass this to the
VNET interface.
The node and network state (bandwidth usage and link state per interface) and various
function specific data (e.g. spanning-tree information) is sent over an
encrypted channel by the VNET agent that runs on the VMs.

IaaS platforms do not always accurately provide topology information.
In those cases, the monitoring controller is capable of deducing the
topology based on platform specific meta-data provided by the VNET agent.
For example, in case of ExoGENI, this meta-data contains the URN that
uniquely identifies the node, the name of the slice,
and the cluster worker node the VM is running on.
ExoGENI however, does not provide a URN representation for its interfaces
to the VMs; therefore, the monitoring controller uses the interface IP
addresses provided by the VNET agent to construct the final topology.


\subsection{Network controller}

VNET interacts with the network components and
changes properties that alter the behaviour of the network and the traffic flows.
The network controller facilitates this;
it uses an RPC channel to the VNET agents to set node and
link configuration, and to execute commands on the nodes.
Changes to the network can be triggered by the user via the multi touch
interface or automatically by VNET.


\subsection{VNET agent}

The VNET agent monitors and controls the node. The daemon maintains secure
connections to both the monitoring controller, and the network controller
over a TLS secured WebSocket\footnote{The WebSocket protocol:
    \url{https://tools.ietf.org/html/rfc6455}} connection.
The agent reports detailed interface statistics and host meta-data such as 
host name, interfaces and IP addresses.
Network function specific data can supplement the host meta-data
by writing this data in the form of (JSON) key/value pairs to a
predefined directory which will be automatically picked up and transmitted to
the monitoring controller.


\subsection{Visualization user interface}

The VNET user interface supports both touch and pointer events, it
is build in JavaScript using the D3.js\footnote{D3.js Data-Driven
    Documents: {https://d3js.org/}} library and uses WebSockets to talk to
the user interface controller component.

\begin{figure}[H]
    \centering
    \includegraphics[width=9cm]{img/sarnet.png}
    \caption{VNET demonstration running scenario: 2 (converted
        to black on white for improved readability)}
    \label{fig:vnet-demo}
\end{figure}

Fig.~\ref{fig:vnet-demo} shows the VNET user interface,
as during the demonstration at SC15.
Information is organised in three columns.
The left column shows information relevant to the operation of the network; 
the demo showed scenario controls and service revenue as described in
Sec.~\ref{sec:collecting}.
The centre column shows a network representation.  It displays disabled
links and the line colour changes based on link utilization for active links. An
observable, node health status, is displayed using a red, orange, or green
circle.  Finally, the right column shows details of the currently selected
node or link, and provides controls for node actions. 

The concurrent display of network and service information is an essential element for the SARNET operation, as this is the only way in which the system can maintain the proper view and balance between effect of software defined network operation and the resulting service to the end users.
%In the view in Fig.~\ref{fig:vnet-demo} you see that Link6 is selected; it shows the details of the link, a graph with transmit and receive traffic, and the user controls for changing the links' properties.


\subsection{Bootstrapping}

To bootstrap a network, we use a topology description, a JSON file with a list
of nodes, node type and a list of all the available network links between these nodes.
The format is kept basic for readability and ease of use.  % The format is shared with/directly usable by the UI.
This is done by using common defaults so properties do not have to be defined for each element.
When supported by the underlying infrastructure, multiple domains can be
used by simply specifying the location (domain) of
the node in the topology.
This simplified topology is converted into the appropriate orchestration
request for the underlying virtualisation platform. In the case of ExoGENI
this is NDL-OWL \cite{van2008distributed,koning2011using,ghijsen2012towards}.  

\subsection{Scenarios}
After bootstrapping the network, we load an attack scenario. The definition
consists of two parts.
First, the initial state of the network is defined, such as which links are
enabled, what their bandwidth is, and which filters are applied.
The scenario also defines the visual elements such as the colours or icons of nodes in the topology.
Secondly, it allows scripting predefined attack patterns over a period of
time.
The script contains a list of time points that define when and by which
nodes the DDoS attacks are started.
Commands contain the target of the attack,
its type (e.g. UDP or TCP),
the duration of the attack,
and its strength in terms of bandwidth.
