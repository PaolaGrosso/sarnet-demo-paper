#!/bin/sh

which inotifywait > /dev/null || exit 1
make

while true
do
    inotifywait -e move_self -e modify *.tex
    sleep .2s
    make
    sleep 1s
done
