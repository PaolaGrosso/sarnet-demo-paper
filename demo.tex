%!TEX root = ./main.tex

\section{Multi-touch table demonstration}
\label{sec:demo}

The demo we showed at the Super Computing 2015 (SC15) conference relies on the prototype described in Sec.~\ref{sec:implementation}.
The visitor of the demo was presented with a multi-touch table interface
showing an interactive visualisation of a network. 
The goal was to use this interface to reconfigure the network and
minimise the effect of the DDoS attack congesting the service and to
recover revenue. By detecting, analysing, deciding and reacting to the
attack, the visitor effectively acts as the SARNET control loop (Fig.~\ref{fig:control-loop}).
The 25 VMs used for this demo were hosted on the \textit{uva-nl} ExoGENI rack and the links between the nodes were requested with a maximum bandwidth of 100 mbit/s.

During SC15, the ExoGENI IaaS platform posed some limitations:
    first, running slices could not be modified\footnote{RENCI deployed slice modification for ExoGENI and
        deployed this early 2016.}. 
    We implemented our own mechanisms to scale bandwidth on the virtual
    links using \textit{tc}\footnote{Tc is a tool to configure Traffic
        Control in the Linux kernel.} on the interfaces of the virtual machines. 
    We used token bucket filter to shape the outgoing rate to the
    bandwidth requested by the visitor. Additionally, removing links in an active slice, was implemented by shutting down interfaces on the virtual machines.
    Secondly, there was no mapping between the interfaces on the virtual
    machines and the interfaces/links in the ExoGENI topology. We solved
    this by using a static IP addressing scheme that allowed us to
    unambiguously identify links and interfaces.

%\begin{itemize}
%%    \tightlist
%    \item Just before SC15 the other ExoGENI racks were upgraded to a newer
%    version; Although tests with the new version were successful, to ensure
%    stability during the event we chose to only run the demo on the
%    \textit{uva-nl} rack.
%    \item We were able to create topologies such that on some racks cause
%    network loops that destabilized the experimental environment; this was
%    another reason to run it on the \textit{uva-nl} rack because the network
%    switch (Ciena 8700) provided the required isolation to prevent these loops
%    from happening.
%\end{itemize}

We used three types of network elements to build the demo, \textit{routers}
running OSPF, \textit{services} and \textit{customers} that can turn into an
\textit{attacker}. \textit{Services} run a web service that simulates web shop
transactions; \textit{customers} send transactions to the web service while
\textit{attackers} send both transactions and attack traffic to the web
services.

\begin{figure}[h]
    \centering
    \includegraphics[width=9cm]{img/scenario.pdf}
    \caption{The two demo scenarios (scenario 1 left and scenario 2 right); colours represent different domains}
    \label{fig:scenarios}
\end{figure}

%%\begin{figure}[H]
%    \centering
%    \includegraphics[width=4.5cm]{img/scenario1.pdf}
%    \caption{First demo scenario}
%    \label{fig:vnet-components}
%\end{figure}

%\begin{figure}[H]
%    \centering
%    \includegraphics[width=4.5cm]{img/scenario2.pdf}
%    \caption{Demo scenario showing two domains depicted as yellow and blue and other (uncontrollable) elements as grey}
%    \label{fig:vnet-components}
%\end{figure}

\label{sec:scenarios}
Fig.~\ref{fig:scenarios} shows the two demo scenarios we used during
SC15 where we varied the number of elements present, the network topology among
them, and the number of domains present.  The network topology is pre-programmed
to ensure a correct mix and spread of attack traffic such that the problem is solvable by the visitor and ensure that all the defence strategies have effect.

In both scenarios in Fig.~\ref{fig:scenarios} virtual customers, C1-12,
attempt to perform transactions with two web services S1-S2.  The
transactions traverse a network consisting of the routers W1-3, N1-4,
E1-3, U1-2 and F1-2. Scenario 1 also includes a switch in the middle, SW1.
Some of these customers are assigned the additional role of attacker and
try to congest the network such that the virtual customers will be unable
to make transactions to the web services. The dual role of both attacker and
consumer is realistic since attack traffic almost always originates from
networks that also send legitimate traffic.

Revenue is determined by summing the successful transactions between
customer and web service in the network. When the attacks start, the visitor sees that the revenue graph
decreases and changes in link utilization. Congestion due to attacks
causes links to change colour and eventually, since traffic cannot reach
the web services, the web service icon becomes red as well.
This is implemented by using an observable on the amount of sales handled
by the service. When the sales drop below a certain threshold, the
observable triggers and changes the web service symbol in the user
interface changes from green to red. When the service recovers, the visualization turns back to green.
The attack traffic consists of UDP
\textit{iperf2}\footnote{We used iperf2 instead of the newer iperf3
    because iperf2 does not require a control channel for UDP.
    \url{https://sourceforge.net/projects/iperf/}}
traffic from multiple hosts at different rates.

%We were also investigating different
%attack traffic using bonesi~\cite{bonesi} yet we did not include this in
%the current version of the demo to focus on the visualisation.

\subsection{Responses and costs}\label{sec:responses}
We implemented four responses that can be applied on all links between
network elements: 1) link \textit{state}, to shut down links; 2) \textit{rate
    up}, to scale bandwidth upward; 3) \textit{rate down}, to scale
bandwidth downward; 4) \textit{filter}, to filter out attack traffic. 
Based on the network display, the visitor can apply one or more of these methods on congested links to restore revenue. The operations have associated costs which are determined as follows:

$$cost = b \frac{ \sum \limits_{i} r_{i}}{2} + f \sum \limits_{i} a_{i} $$

\begin{itemize}
    \tightlist
    \item where $b$ is interface cost in \$ per megabit; we used $b=10$
    \item where $i$ is an active interface
    \item where $r_i$ is link bandwidth of interface $i$ in Mbit/s
    \item where $f$ is the cost of a filter in \$; we used $f=500$
    \item where $a_i$ is $1$ if a filter is active on interface $i$ or $0$
\end{itemize}

The value for $b$ was chosen based on the consideration that bandwidth cost in
North-America is \$10 per megabit per second\cite{bwcost}. The value for the parameter
$f$ derives from the observation that the market offers DDoS mitigation services from a few hundred to thousands of dollars per month; we therefore chose \$500 for a filter action since it is 100\% effective when applied to a link. Because in this demonstration the
number of nodes is fixed, we did not add node costs to the equation.

\subsection{Collecting solutions}\label{sec:collecting}
To collect defence strategies from all visitors operating the system,
we limited the time for each visitor to 4 minutes; during this time,
the visitor's goal is to minimise the attack and maximise the sales.
Fig.~\ref{fig:vnet-demo} on the top-left, shows the
controls for the demonstration. Reset stops the attack and starts
with a clean scenario. \textit{Start}/\textit{Retry} lets the visitor retry the problem
without submitting. \textit{Done}/\textit{Submit} ends the timer and submits the final result.
When the visitor presses \textit{done}, the revenue over a small time window is measured and this is considered the final score. \textit{Submit} will save the solution.

Per visitor we stored a unique session id and the information listed in
Table~\ref{tbl:scenario1} and \ref{tbl:scenario2}.
