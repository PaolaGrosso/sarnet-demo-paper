
PAPER = sarnet-demo-paper

.PHONY: clean ${PAPER}.pdf

pdf: ${PAPER}.pdf

${PAPER}.pdf:
	#bibtex ${PAPER}
	latexmk -pdf -halt-on-error -bibtex -jobname="${PAPER}" main.tex > /dev/null

clean:
	latexmk -C -bibtex -jobname="${PAPER}" main.tex 
